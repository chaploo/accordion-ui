<?php

/**
 * @module          Accordion-UI
 * @author          ISB
 * @copyright       2019-2023 ISB
 * @link            https://www.internet-service-berlin.de
 * @license         GNU General Public License 3 (see info.php)
 * @license_terms   see license
 *
 */

// include secure.php to protect this file and the whole CMS!
if (defined('LEPTON_PATH')) {	
	include(LEPTON_PATH.'/framework/class.secure.php'); 
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.'/framework/class.secure.php'))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.'/framework/class.secure.php')) { 
		include($root.'/framework/class.secure.php'); 
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure.php

$MOD_ACCORDION_UI = array(
	'action'	    => "Aktion",
	'add'	    	=> "Eintrag hinzufügen",
	'content'	    => "Inhalt",
	'delete_ok'     => "Datensatz erfolgreich gelöscht",
	'duplicate'     => "Kopieren",
	'edit'	        => "Bearbeiten",
	'info'	        => "Addon Info",
	'header1'	    => "ID",
	'header2'	    => "Titel",
	'header3'	    => "Aktiv",
	'header4'	    => "Bearbeiten",
	'header5'	    => "Kopieren",
	'header6'	    => "Löschen",
	'help'	    	=> "Hilfe-Seite",
	'list_header1'	=> "Hinzufügen/Ändern Beitrag",
	'open'	    	=> "Element geöffnet darstellen",
	'save_ok'	    => "Daten erfolgreich gespeichert",
	'to_delete'	    => "wirklich löschen",
	'want_delete'	=> "Wollen Sie den Datensatz",

);

?>