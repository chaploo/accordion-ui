<?php

/**
 * @module          Accordion-UI
 * @author          ISB
 * @copyright       2019-2023 ISB
 * @link            https://www.internet-service-berlin.de
 * @license         GNU General Public License 3 (see info.php)
 * @license_terms   see license
 *
 */
 
class accordion_ui extends LEPTON_abstract
{
	public $all_accordions = array();
	public $database = 0;
	public $admin = 0;
	public $addon_color = 'olive';
	public $action = ADMIN_URL.'/pages/modify.php?page_id=';
	public $mod_action = LEPTON_URL.'/modules/accordion_ui/modify.php';	
		
	
	public static $instance;	
	
	public function initialize() 
	{
		$this->database = LEPTON_database::getInstance();		
		$this->admin = LEPTON_admin::getInstance();	
	}
	
	public function init_section( $iPageID = 0, $iSectionID = 0 )
	{
		//get array of all_accordions
		$this->all_accordions = array();
		$this->database->execute_query(
			"SELECT * FROM ".TABLE_PREFIX."mod_accordion_ui WHERE page_id=". $iPageID." AND section_id=". $iSectionID." ORDER BY position ASC",
			true,
			$this->all_accordions,
			true
		);		
	}

	public function show_info()
	{
		$support_link = '<a href="#">NO Live-Support / FAQ</a>';	
		$readme_link = '<a href="https://www.internet-service-berlin.de/webdesign/module/accordion.php" class="extern" target="_blank">Readme</a>';
		$page_id = $_POST['show_info'];
		
		$form_values = array(
			'image'			=> "https://www.internet-service-berlin.de/media/module/accordion.gif",// 201x201px
			'oAC'			=> $this,
			'readme_link'	=> $readme_link,
			'page_id'		=> $page_id,
			'SUPPORT'		=> $support_link,	
			'leptoken'		=> get_leptoken()
			
		);

		/**	
		 *	get the template-engine.
		 */
		$oTwig = lib_twig_box::getInstance();
		$oTwig->registerModule('accordion_ui');
			
		echo $oTwig->render( 
			"@accordion_ui/info.lte",	//	template-filename
			$form_values			//	template-data
		);	
		exit();		
	}	

	public function edit_accordion( $iAccordionId = 0 )
	{
		if(isset($_POST['edit_accordion'] ) && $_POST['edit_accordion'] != 0 )
		{	
			$accordion_id = intval( $_POST['edit_accordion']);
			
			// get current accordion from database
			$current_accordion = array();
			$this->database->execute_query(
				"SELECT * FROM ".TABLE_PREFIX."mod_accordion_ui WHERE id = ".$accordion_id." ",
				true,
				$current_accordion,
				false
			);	

			//create wysiwyg : show_wysiwyg_editor($name,$id,$content,$width,$height, $prompt)
			require(LEPTON_PATH.'/modules/'.WYSIWYG_EDITOR.'/include.php');
			$show_wysiwyg = show_wysiwyg_editor('content','ac_content',$current_accordion['content'],'100%','250',false);				
		
			$form_values = array(
				'oAC'				=> $this,
				'current_accordion'	=> $current_accordion,
				'show_wysiwyg'		=> $show_wysiwyg,				
				'leptoken'			=> get_leptoken()
				
			);

			/**	
			 *	get the template-engine.
			 */
			$oTwig = lib_twig_box::getInstance();
			$oTwig->registerModule('accordion_ui');
				
			echo $oTwig->render( 
				"@accordion_ui/accordion_detail.lte",	//	template-filename
				$form_values			//	template-data
			);		
			exit();
		}
	
	} 
	
	public function save_accordion( $iAccordionId = 0 )
	{
		
		if(isset($_POST['save_accordion'] ) && $_POST['save_accordion'] != 0 )
		{
			$accordion_id = intval($_POST['save_accordion']);
			$_POST['title'] = strip_tags($_POST['title']);
			$request = LEPTON_request::getInstance();	

			if(!isset($_POST['active']))
			{
				$_POST['active'] = 0;
			}
			else
			{
				$_POST['active'] = $_POST['active'];
			}
			
			$all_names = array (
				'title'			=> array ('type' => 'str', 'default' => "", 'range' =>""),
				'content'		=> array ('type' => 'str', 'default' => "", 'range' =>""),
				'active'		=> array ('type' => 'int', 'default' => "1", 'range' =>"")
			);		

			$all_values = $request->testPostValues($all_names);			
			$table = TABLE_PREFIX."mod_accordion_ui";
			$result = $this->database->build_and_execute( 'UPDATE', $table, $all_values,'id = '.$accordion_id.' ');	

			if($result == false) {
				die (LEPTON_tools::display($this->database->get_error(),'pre','ui red message'));
				exit(0);
			}
			else 
			{
				// save = ok and forward to start screen
				echo(LEPTON_tools::display($this->language['save_ok'],'pre','ui green message'));
				header("Refresh:1; url=".$this->action.$_POST['page_id']."&leptoken=".$_POST['leptoken']."");
				exit(0);
			}				
			
			
		}
	}
	
	public function duplicate_accordion( $iAccordionId = 0 )
	{
		if(isset($_POST['duplicate_accordion'] ) && $_POST['duplicate_accordion'] != 0 )
		{
			$accordion_id = intval($_POST['duplicate_accordion']);

			// get current accordion from database
			$current_accordion = array();
			$this->database->execute_query(
				"SELECT * FROM ".TABLE_PREFIX."mod_accordion_ui WHERE id = ".$accordion_id." ",
				true,
				$current_accordion,
				false
			);	
			
			
			// Get new order
			$order = new LEPTON_order(TABLE_PREFIX.'mod_accordion_ui', 'position', 'id', 'section_id');
			$position = $order->get_new($current_accordion['section_id']);			

			$fields = array(
				'id' => NULL,
				'page_id' => $current_accordion['page_id'],
				'section_id' => $current_accordion['section_id'],
				'title' => $current_accordion['title']."-Kopie",
				'content' => $current_accordion['content'],
				'position' => $position
			);
			
			$result = $this->database->build_and_execute (
				"INSERT",
				TABLE_PREFIX."mod_accordion_ui",
				$fields
			);

			if($result == false) {
				die (LEPTON_tools::display($this->database->get_error(),'pre','ui red message'));
				exit(0);
			}
			else 
			{
				// save = ok and forward to start screen
				echo(LEPTON_tools::display($this->language['save_ok'],'pre','ui green message'));
				header("Refresh:1; url=".$this->action.$_POST['page_id']."&leptoken=".$_POST['leptoken']."");
				exit(0);
			}				
		}
	}

	public function add_accordion( $iAccordionId = 0 )
	{
		if(isset($_POST['add_accordion'] ) && $_POST['add_accordion'] != 0 )
		{
			$section_id = intval($_POST['add_accordion']);
			$page_id = intval($_POST['page_id']);
			$max_position = $this->database->get_one("SELECT MAX(position) as id FROM ".TABLE_PREFIX."mod_accordion_ui WHERE section_id = ".$section_id);
			if($max_position == NULL)
			{
				$position = 1;
			}
			else
			{
				$position = $max_position + 1;
			}

			$fields = array(
				'id' => NULL,
				'page_id' => $page_id,
				'section_id' => $section_id,
				'title' => "Neuer Eintrag",
				'content' => "Text",
				'position' => $position
			);
			
			$result = $this->database->build_and_execute (
				"INSERT",
				TABLE_PREFIX."mod_accordion_ui",
				$fields
			);

			if($result == false) {
				die (LEPTON_tools::display($this->database->get_error(),'pre','ui red message'));
				exit(0);
			}
			else 
			{
				// save = ok and forward to start screen
				echo(LEPTON_tools::display($this->language['save_ok'],'pre','ui green message'));
				header("Refresh:1; url=".$this->action.$_POST['page_id']."&leptoken=".$_POST['leptoken']."");
				exit(0);
			}				
		}
	}

	public function move_down( $iAccordionId = 0 )
	{
		if(isset($_POST['move_down'] ) && $_POST['move_down'] != 0 )
		{
			$accordion_id = intval($_POST['move_down']);
			$section_id = $this->database->get_one("SELECT section_id FROM ".TABLE_PREFIX."mod_accordion_ui WHERE id = ".$accordion_id." ");	
		
			// Create new order object an reorder
			$order = new LEPTON_order(TABLE_PREFIX.'mod_accordion_ui', 'position', 'id', 'section_id');
			if($order->move_down($accordion_id)) 
			{
				// Clean up ordering
				$order = new LEPTON_order(TABLE_PREFIX.'mod_accordion_ui', 'position', 'id', 'section_id');
				$order->clean($section_id); 
				
				// save = ok and forward to start screen
				echo(LEPTON_tools::display($this->language['save_ok'],'pre','ui green message'));
				header("Refresh:1; url=".$this->action.$_POST['page_id']."&leptoken=".$_POST['leptoken']."");
				exit(0);
			} else 
			{
				die (LEPTON_tools::display($this->database->get_error(),'pre','ui red message'));
				exit(0);
			}			
			
		}
	}	


	public function move_up( $iAccordionId = 0 )
	{
		if(isset($_POST['move_up'] ) && $_POST['move_up'] != 0 )
		{
			$accordion_id = intval($_POST['move_up']);
			$section_id = $this->database->get_one("SELECT section_id FROM ".TABLE_PREFIX."mod_accordion_ui WHERE id = ".$accordion_id." ");	
		
			// Create new order object an reorder
			$order = new LEPTON_order(TABLE_PREFIX.'mod_accordion_ui', 'position', 'id', 'section_id');
			if($order->move_up($accordion_id)) 
			{
				// Clean up ordering
				$order = new LEPTON_order(TABLE_PREFIX.'mod_accordion_ui', 'position', 'id', 'section_id');
				$order->clean($section_id); 
				
				// save = ok and forward to start screen
				echo(LEPTON_tools::display($this->language['save_ok'],'pre','ui green message'));
				header("Refresh:1; url=".$this->action.$_POST['page_id']."&leptoken=".$_POST['leptoken']."");
				exit(0);
			} else 
			{
				die (LEPTON_tools::display($this->database->get_error(),'pre','ui red message'));
				exit(0);
			}			
			
		}
	}	
	
	
	public function delete_accordion( $iAccordionId = 0 )
	{
		if(isset($_POST['delete_accordion'] ) && $_POST['delete_accordion'] != 0 )
		{
			$accordion_id = intval($_POST['delete_accordion']);
			$section_id = $this->database->get_one("SELECT section_id FROM ".TABLE_PREFIX."mod_accordion_ui WHERE id = ".$accordion_id." ");	

			$result = $this->database->simple_query("DELETE FROM ".TABLE_PREFIX."mod_accordion_ui WHERE id = ".$accordion_id." ");	

			if($result == false) {
				die (LEPTON_tools::display($this->database->get_error(),'pre','ui red message'));
				exit(0);
			}
			else 
			{
				// Clean up ordering
				$order = new LEPTON_order(TABLE_PREFIX.'mod_accordion_ui', 'position', 'id', 'section_id');
				$order->clean($section_id); 
				
				// save = ok and forward to start screen
				echo(LEPTON_tools::display($this->language['delete_ok'],'pre','ui green message'));
				header("Refresh:1; url=".$this->action.$_POST['page_id']."&leptoken=".$_POST['leptoken']."");
				exit(0);
			}				
			
			
		}
	}

	public function open_accordion( $iAccordionId = 0 )
	{
		if(isset($_POST['open_accordion'] ) && $_POST['open_accordion'] != 0 )
		{
			$accordion_id = intval($_POST['open_accordion']);
			
			$section_id = $this->database->get_one("SELECT section_id FROM ".TABLE_PREFIX."mod_accordion_ui WHERE id = ".$accordion_id." ");	
			$open_old = $this->database->get_one("SELECT id FROM ".TABLE_PREFIX."mod_accordion_ui WHERE section_id = ".$section_id." and open = 1 ");
			
			if ($open_old > 0)
			{
				$this->database->simple_query("UPDATE ".TABLE_PREFIX."mod_accordion_ui SET open = 0 WHERE id = ".$open_old." ");	
			}
		
			if( $accordion_id != $open_old) // only inactive gets activated
			{
				$this->database->simple_query("UPDATE ".TABLE_PREFIX."mod_accordion_ui SET open = 1 WHERE id = ".$accordion_id." ");	
			}			

			// save = ok and forward to start screen
			echo(LEPTON_tools::display($this->language['save_ok'],'pre','ui green message'));
			header("Refresh:1; url=".$this->action.$_POST['page_id']."&leptoken=".$_POST['leptoken']."");
			exit(0);
		}
	}
}