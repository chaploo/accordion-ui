<?php

/**
 * @module          Accordion-UI
 * @author          ISB
 * @copyright       2019-2023 ISB
 * @link            https://www.internet-service-berlin.de
 * @license         GNU General Public License 3 (see info.php)
 * @license_terms   see license
 *
 */
 
class accordion_ui_frontend extends LEPTON_abstract
{
	public $database = 0;
	public $all_accordions = array();
	public $action_url = LEPTON_URL.'/modules/accordion_ui/';	
	public $view_url = LEPTON_URL.PAGES_DIRECTORY;
		
	
	public static $instance;

	public function initialize() 
	{
		$this->database = LEPTON_database::getInstance();
		$this->init_page();
	}
	
	public function init_page( $iPageID = 0, $iSectionID = 0 )
	{
		$page_link = $this->database->get_one("SELECT link FROM ".TABLE_PREFIX."pages WHERE page_id=". $iPageID."");
		$this->view_url = LEPTON_URL.PAGES_DIRECTORY.$page_link.PAGE_EXTENSION;
		
		//get array of all_accordions
		$this->all_accordions = array();
		$this->database->execute_query(
			"SELECT * FROM ".TABLE_PREFIX."mod_accordion_ui WHERE section_id=". $iSectionID." AND active = 1 ORDER BY position ASC ",
			true,
			$this->all_accordions,
			true
		);			
	}		
}
