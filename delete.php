<?php

/**
 * @module          Accordion-UI
 * @author          ISB
 * @copyright       2019-2023 ISB
 * @link            https://www.internet-service-berlin.de
 * @license         GNU General Public License 3 (see info.php)
 * @license_terms   see license
 *
 */
 
// include secure.php to protect this file and the whole CMS!
if (defined('LEPTON_PATH')) {   
   include(LEPTON_PATH.'/framework/class.secure.php');
} else {
   $oneback = "../";
   $root = $oneback;
   $level = 1;
   while (($level < 10) && (!file_exists($root.'/framework/class.secure.php'))) {
      $root .= $oneback;
      $level += 1;
   }
   if (file_exists($root.'/framework/class.secure.php')) {
      include($root.'/framework/class.secure.php');
   } else {
      trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
   }
}
// end include secure.php


//get instance of own module class
//$oAC = accordion::getInstance();

$table = TABLE_PREFIX."mod_accordion_ui";
$data = array();
$database->execute_query(
	"SELECT * from ". $table ." where section_id = ".$section_id." ",
	true,
	$data,
	true
);	

// echo (LEPTON_tools::display($data,'pre','ui message'));

if (count($data) > 0 ) {
	// delete accordion from table
	$database->simple_query("DELETE from ".$table." where section_id = ".$section_id);
}

?>