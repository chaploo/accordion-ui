<?php

/**
 * @module          Accordion-UI
 * @author          ISB
 * @copyright       2019-2023 ISB
 * @link            https://www.internet-service-berlin.de
 * @license         GNU General Public License 3 (see info.php)
 * @license_terms   see license
 *
 */

// include secure.php to protect this file and the whole CMS!
if (defined('LEPTON_PATH')) {	
	include(LEPTON_PATH.'/framework/class.secure.php'); 
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.'/framework/class.secure.php'))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.'/framework/class.secure.php')) { 
		include($root.'/framework/class.secure.php'); 
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure.php


$debug = true;

if (true === $debug) {
	ini_set('display_errors', 1);
	error_reporting(E_ALL);
}

//get instance of own module class
$oAC = accordion_ui::getInstance();
$oAC->init_section( $page_id, $section_id );

//die(LEPTON_tools::display($_POST,'pre','ui message'));


if(isset($_POST['show_info'] ))
{
	$oAC->show_info();
} 
elseif(isset($_POST['edit_accordion'] ) && $_POST['edit_accordion'] != 0 )
{
		$oAC->edit_accordion($_POST['edit_accordion']);
}
elseif(isset($_POST['save_accordion'] ) && $_POST['save_accordion'] != 0 )
{
		$oAC->save_accordion($_POST['save_accordion']);
}
elseif(isset($_POST['duplicate_accordion'] ) && $_POST['duplicate_accordion'] != 0 )
{
		$oAC->duplicate_accordion($_POST['duplicate_accordion']);
}
elseif(isset($_POST['add_accordion'] ) && $_POST['add_accordion'] != 0 )
{
		$oAC->add_accordion($_POST['add_accordion']);
}
elseif(isset($_POST['delete_accordion'] ) && $_POST['delete_accordion'] != 0 )
{
		$oAC->delete_accordion($_POST['delete_accordion']);
}
elseif(isset($_POST['move_down'] ) && $_POST['move_down'] != 0 )
{
		$oAC->move_down($_POST['move_down']);
}
elseif(isset($_POST['move_up'] ) && $_POST['move_up'] != 0 )
{
		$oAC->move_up($_POST['move_up']);
}
elseif(isset($_POST['open_accordion'] ) && $_POST['open_accordion'] != 0 )
{
		$oAC->open_accordion($_POST['open_accordion']);
}
else
{	
	$min_position = $oAC->database->get_one(" SELECT MIN(position) FROM ".TABLE_PREFIX."mod_accordion_ui WHERE section_id = ".$section_id." ");
	$max_position =	$oAC->database->get_one(" SELECT MAX(position) FROM ".TABLE_PREFIX."mod_accordion_ui WHERE section_id = ".$section_id." ");

	$form_values = array(
		'min_pos'		=> $min_position,
		'max_pos'		=> $max_position,
		'oAC'			=> $oAC,
		'section_id'	=> $section_id,
		'page_id'		=> $page_id,
		'read_me'		=> 'https://www.internet-service-berlin.de/webdesign/module/accordion.php',
		'leptoken'		=> get_leptoken()
		
	);
	
	 //	get the template-engine.
	$oTwig = lib_twig_box::getInstance();
	$oTwig->registerModule('accordion_ui');
		
	echo $oTwig->render( 
		"@accordion_ui/modify.lte",	//	template-filename
		$form_values			//	template-data
	);
	
}
?>