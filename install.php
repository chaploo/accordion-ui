<?php

/**
 * @module          Accordion-UI
 * @author          ISB
 * @copyright       2019-2023 ISB
 * @link            https://www.internet-service-berlin.de
 * @license         GNU General Public License 3 (see info.php)
 * @license_terms   see license
 *
 */

// include secure.php to protect this file and the whole CMS!
if (defined('LEPTON_PATH')) {	
	include(LEPTON_PATH.'/framework/class.secure.php'); 
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.'/framework/class.secure.php'))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.'/framework/class.secure.php')) { 
		include($root.'/framework/class.secure.php'); 
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure.php


// install table
$table_fields="
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`page_id` int(11) NOT NULL DEFAULT '-1',
	`section_id` int(11) NOT NULL DEFAULT '-1',
	`title` varchar(256) NOT NULL DEFAULT 'none',
	`content` text,
	`position` int(11) NOT NULL DEFAULT '0',
	`open` int(1) NOT NULL DEFAULT '0',
	`active` int(1) NOT NULL DEFAULT '1',
	PRIMARY KEY ( `id` )
	";
LEPTON_handle::install_table("mod_accordion_ui", $table_fields);

?>