<?php

/**
 * @module          Accordion-UI
 * @author          ISB
 * @copyright       2019-2023 ISB
 * @link            https://www.internet-service-berlin.de
 * @license         GNU General Public License 3 (see info.php)
 * @license_terms   see license
 *
 */
 
 // include secure.php to protect this file and the whole CMS!
if (defined('LEPTON_PATH')) {   
   include(LEPTON_PATH.'/framework/class.secure.php');
} else {
   $oneback = "../";
   $root = $oneback;
   $level = 1;
   while (($level < 10) && (!file_exists($root.'/framework/class.secure.php'))) {
      $root .= $oneback;
      $level += 1;
   }
   if (file_exists($root.'/framework/class.secure.php')) {
      include($root.'/framework/class.secure.php');
   } else {
      trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
   }
}
// end include secure.php


$mod_headers = array(
	'backend' => array(
        'css' => array(
            array(
                'media'  => 'all',
                'file'  => 'modules/lib_fomantic/dist/semantic.min.css'
			)
 		),				
		'js' => array(
			'modules/lib_jquery/jquery-core/jquery-core.min.js',
			'modules/lib_jquery/jquery-core/jquery-migrate.min.js',			
			'modules/lib_fomantic/dist/semantic.min.js'
		)
	),
	'frontend' => array(
        'css' => array(
            array(
                'media'  => 'all',
                'file'  => 'modules/lib_jquery/jquery-ui/jquery-ui.min.css'
			)
 		),		
		'js' => array(
			'modules/lib_jquery/jquery-core/jquery-core.min.js',
			'modules/lib_jquery/jquery-core/jquery-migrate.min.js',			
			'modules/lib_jquery/jquery-ui/jquery-ui.min.js'
		)
	)	
);

?>