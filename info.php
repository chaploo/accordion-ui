<?php

/**
 * @module          Accordion-UI
 * @author          ISB
 * @copyright       2019-2023 ISB
 * @link            https://www.internet-service-berlin.de
 * @license         GNU General Public License 3 (see info.php)
 * @license_terms   see license
 *
 */

// include secure.php to protect this file and the whole CMS!
if (defined('LEPTON_PATH')) {	
	include(LEPTON_PATH.'/framework/class.secure.php'); 
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.'/framework/class.secure.php'))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.'/framework/class.secure.php')) { 
		include($root.'/framework/class.secure.php'); 
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure.php

$module_directory     = 'accordion_ui';
$module_name          = 'Accordion-UI';
$module_function      = 'page';
$module_version       = '1.1.0';
$module_platform      = '7.x';
$module_author        = 'ISB';
$module_home          = 'https://www.internet-service-berlin.de';
$module_guid          = 'b74f6c95-a2a3-4417-a28b-9d2d7cfad348';
$module_license       = '<a href="https://www.gnu.org/licenses/gpl-3.0" target="_blank">GNU General Public License 3</a>';
$module_license_terms = 'no license terms';
$module_description   = 'Displays accordions using <a href="https://jqueryui.com/accordion/" target="_blank">jQuery UI</a>';

