<?php

/**
 * @module          Accordion-UI
 * @author          ISB
 * @copyright       2019-2023 ISB
 * @link            https://www.internet-service-berlin.de
 * @license         GNU General Public License 3 (see info.php)
 * @license_terms   see license
 *
 */
 
// include secure.php to protect this file and the whole CMS!
if (defined('LEPTON_PATH')) {   
   include(LEPTON_PATH.'/framework/class.secure.php');
} else {
   $oneback = "../";
   $root = $oneback;
   $level = 1;
   while (($level < 10) && (!file_exists($root.'/framework/class.secure.php'))) {
      $root .= $oneback;
      $level += 1;
   }
   if (file_exists($root.'/framework/class.secure.php')) {
      include($root.'/framework/class.secure.php');
   } else {
      trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
   }
}
// end include secure.php

//echo(LEPTON_tools::display($_POST,'pre','ui message'));
//get instance of own module class
$oACF = accordion_ui_frontend::getInstance();
$oACF->init_page( $page_id, $section_id);
if(empty($_POST))  
{
	$open_position = $oACF->database->get_one("SELECT position from ".TABLE_PREFIX."mod_accordion_ui WHERE section_id = ".$section_id." AND open = 1 ");
	// marker settings
	$form_values = array(
		'oACF'			=> $oACF,
		'section_id'	=> $section_id,
		'page_id'		=> $page_id,
		'open_position'	=> $open_position - 1,	
		'leptoken'		=> get_leptoken()
	);

	/**	
	 *	get the template-engine.
	 */
	$oTwig = lib_twig_box::getInstance();
	$oTwig->registerModule('accordion_ui');
		
	echo $oTwig->render( 
		"@accordion_ui/view.lte",	//	template-filename
		$form_values				//	template-data
	);

}

?>