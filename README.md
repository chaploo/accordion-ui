### Accordion-UI
=========

Displays accordions using [jQuery UI][3].


#### Requirements

* [LEPTON CMS][1], Version see precheck.php
* include jquery and jquer-ui in your frontend template (use headers.inc.php)
 

#### Installation

* download latest [Accordion-UI.zip][2] installation archive
* in CMS backend select the file from "Add-ons" -> "Modules" -> "Install module"

#### Notice

After installing addon create a page or section using accordion-ui module. <br />
For further details please see [readme file][4].


[1]: https://lepton-cms.org "LEPTON CMS"
[2]: http://www.lepton-cms.com/lepador/modules/accordion_ui.php
[3]: https://jqueryui.com/accordion/
[4]: https://www.internet-service-berlin.de/_documentation/accordion_ui/readme.php

### Image Credits

Module Icon "Filmstrip" by Gerd Altman/Pixabay
