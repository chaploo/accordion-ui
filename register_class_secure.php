<?php

/**
 * @module          Accordion-UI
 * @author          ISB
 * @copyright       2019-2023 ISB
 * @link            https://www.internet-service-berlin.de
 * @license         GNU General Public License 3 (see info.php)
 * @license_terms   see license
 *
 */
 

$files_to_register = array(
	'modify.php'
);

LEPTON_secure::getInstance()->accessFiles($files_to_register);

?>